package tech.mastertech.itau.lancamento;

import java.time.LocalDateTime;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

@Service
public class LancamentoService {
	@Autowired
	private LancamentoRepository lancamentoRepository;

	@Autowired
	private CartaoClient cartaoService;

	public Lancamento criar(Lancamento lancamento) {
		String cartao = lancamento.getIdCartao();

		Cartao optional = cartaoService.buscar(cartao);

		if (optional == null) {
//      throw new ValidacaoException("cartao", "Cartão não encontrado");
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Cartão não encontrado");
		}

		lancamento.setHorario(LocalDateTime.now());

		if (cartaoService.buscar(cartao) != null) {
			return lancamentoRepository.save(lancamento);
		} else {
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Senha inválida");
		}

	}

	public Iterable<Lancamento> buscarTodasPorNumeroDoCartao(String numero) {
		return lancamentoRepository.findAllByIdCartao(numero);
	}

}
