package tech.mastertech.itau.lancamento;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;



@RestController
public class LancamentoController {
	
	@Autowired
	LancamentoService lancamentoService;
	
	@Autowired
	CartaoClient cartaoService;
	
	Logger logger = LoggerFactory.getLogger(LancamentoController.class);

	@PostMapping("/lancamento")
	public Lancamento setLancamento(@RequestBody Lancamento lancamento) {
		return lancamentoService.criar(lancamento);
	}
	
	@HystrixCommand(fallbackMethod = "contingenciaTeste")
	@GetMapping("/teste")
	public String teste() {
		return cartaoService.teste();
	}
	
	public String contingenciaTeste(Throwable e) {
		logger.error(e.getMessage());
		return "Eu sou a contingência";
	}
}
