package tech.mastertech.itau.lancamento;

import javax.validation.Valid;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseStatus;

@FeignClient(name = "cartao", fallback = CartaoFallback.class)
public interface CartaoClient {
	
	@PostMapping("/cartao")
	@ResponseStatus(code = HttpStatus.CREATED)
	public Cartao criar(@Valid @RequestBody Cartao idCartao);
	
	@PatchMapping("/cartao/{numero}/ativar")
	public void ativar(@PathVariable String numero) ;
	
	@GetMapping("/cartao/{numero}")
	public Cartao buscar(@PathVariable String numero);
	
	@GetMapping("/cartao/teste")
	public String teste();
	
}
