package tech.mastertech.itau.logs.controllers;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import tech.mastertech.itau.logs.dtos.Log;
import tech.mastertech.itau.logs.services.ClienteService;

@RestController
public class ClienteController {

	@Autowired
	private ClienteService clienteService;

	@GetMapping("/cliente")
	public Iterable<Log> listarClientesPorData(@RequestBody String data) {
		System.out.println(data);
		LocalDate dataFinal = LocalDate.parse(data, DateTimeFormatter.ISO_LOCAL_DATE );
		return clienteService.buscarPorData(dataFinal);
	}

}
