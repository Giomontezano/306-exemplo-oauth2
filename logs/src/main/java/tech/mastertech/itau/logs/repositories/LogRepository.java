package tech.mastertech.itau.logs.repositories;

import java.time.LocalDate;

import org.springframework.data.cassandra.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import tech.mastertech.itau.logs.dtos.Log;

public interface LogRepository extends CrudRepository<Log, Integer>{
	
	public Iterable<Log> findAllByData(LocalDate data);
	
	@Query("select a from log a where a.data <= :data")
    Iterable<Log> findAllWithDataBefore(@Param("data") LocalDate data);
}