package tech.mastertech.itau.logs.dtos;

import java.time.LocalDate;
import java.util.UUID;

import org.springframework.data.cassandra.core.cql.PrimaryKeyType;
import org.springframework.data.cassandra.core.mapping.PrimaryKeyColumn;
import org.springframework.data.cassandra.core.mapping.Table;

@Table
public class Log {

	@PrimaryKeyColumn(type = PrimaryKeyType.CLUSTERED)
	private UUID registro;
	@PrimaryKeyColumn(type = PrimaryKeyType.PARTITIONED)
	private LocalDate data;
	private String descricao;

	public LocalDate getData() {
		return data;
	}

	public void setData(LocalDate data) {
		this.data = data;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public UUID getRegistro() {
		return registro;
	}

	public void setRegistro(UUID registro) {
		this.registro = registro;
	}

}
