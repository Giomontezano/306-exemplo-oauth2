package tech.mastertech.itau.logs.services;

import java.io.IOException;
import java.time.LocalDate;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import tech.mastertech.itau.logs.dtos.Cartao;
import tech.mastertech.itau.logs.dtos.Cliente;
import tech.mastertech.itau.logs.dtos.Log;
import tech.mastertech.itau.logs.repositories.LogRepository;

@Component
public class ClienteService {

	@Autowired
	private LogRepository logRepository;
	
	private ObjectMapper mapper = new ObjectMapper();

	@KafkaListener(id = "clienteconsumer", topics = "criar-cliente")
	public void criarCliente(@Payload String str) throws JsonParseException, JsonMappingException, IOException {
		
		Cliente cliente = mapper.readValue(str, Cliente.class);
		

		
		Log log = new Log();
		log.setRegistro(UUID.randomUUID());
		log.setData(LocalDate.now());
		log.setDescricao("Cliente: cliente "+ cliente.getNome() + " criado");

		logRepository.save(log);

	}

	@KafkaListener(id = "cartaoconsumer", topics = "criar-cartao")
	public void criarCartao(@Payload String str) throws JsonParseException, JsonMappingException, IOException {
		Cartao cartao = mapper.readValue(str, Cartao.class);

		Log log = new Log();
		log.setRegistro(UUID.randomUUID());
		log.setData(LocalDate.now());
		log.setDescricao("Cartão: cartão " + cartao.getNumero() + " criado");

		logRepository.save(log);

	}

	@KafkaListener(id = "cartaoativoconsumer", topics = "ativar-cartao")
	public void ativarCartao(@Payload String str) throws Exception, JsonMappingException, IOException {
		Cartao cartao = mapper.readValue(str, Cartao.class);
		
		Log log = new Log();
		log.setRegistro(UUID.randomUUID());
		log.setData(LocalDate.now());
		log.setDescricao("Cartão: cartão " + cartao.getNumero() + " ativado");

		logRepository.save(log);

	}
	
	public Iterable<Log> buscarPorData(LocalDate data){
		
		return logRepository.findAllByData(data);
	}
	
}
