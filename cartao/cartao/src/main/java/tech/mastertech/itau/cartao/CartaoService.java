package tech.mastertech.itau.cartao;

import java.util.Optional;
import java.util.Random;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

@Service
public class CartaoService {
	@Autowired
	private CartaoRepository cartaoRepository;
	
	@Autowired
	private ClienteClient clienteClient;

	public Cartao criar(Cartao cartao) {
		
		Optional<Cliente> cliente = clienteClient.buscar(cartao.getCpfCliente());

		if (!cliente.isPresent()) {
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST,"Cliente não encontrado");
		}

		cartao.setNumero(gerarNumero());
		cartao.setAtivo(false);

		return cartaoRepository.save(cartao);
	}

	public Optional<Cartao> buscar(String numero) {
		return cartaoRepository.findById(numero);
	}

	private String gerarNumero() {
		Random random = new Random();
		String numero = "";

		for (int i = 0; i < 16; i++) {
			numero += random.nextInt(9);
		}

		if (cartaoRepository.findById(numero).isPresent()) {
			return gerarNumero();
		}

		return numero;
	}

	public void ativar(String numero) {
		Optional<Cartao> cartaoOptional = cartaoRepository.findById(numero);

		if (!cartaoOptional.isPresent()) {
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Cartão não encontrado");
		}

		Cartao cartao = cartaoOptional.get();

		if (cartao.getAtivo()) {
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Cartão já está ativo");
		}

		cartao.setAtivo(true);
		cartaoRepository.save(cartao);
	}

	public Cartao autenticarCartao(Cartao cartao) {
		Optional<Cartao> cartaoopt = this.buscar(cartao.getNumero());

		if (cartaoopt == null) {
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
		}

		BCryptPasswordEncoder bPasswordEncoder = new BCryptPasswordEncoder();

		if (bPasswordEncoder.matches(cartao.getSenha(), cartaoopt.get().getSenha())) {
			return cartao;
		}

		return null;
	}
}
