package tech.mastertech.itau.cartao;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

@FeignClient(name = "authserver")
public interface AuthClient {

	@PostMapping("/cartao")
	public void criarUsuario(@RequestBody Usuario usuario);
}
