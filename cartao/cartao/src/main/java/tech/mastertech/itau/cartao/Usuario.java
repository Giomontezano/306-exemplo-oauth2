package tech.mastertech.itau.cartao;

import javax.persistence.Entity;
import javax.persistence.Id;

import org.hibernate.validator.constraints.br.CPF;

@Entity
public class Usuario {
	
	@Id
	@CPF
	private String cpf;

	public String getCpf() {
		return cpf;
	}

	public void setCpf(String cpf) {
		this.cpf = cpf;
	}

}
