package tech.mastertech.itau.cartao;

import java.security.Principal;
import java.util.Optional;

import javax.validation.Valid;

import org.apache.kafka.clients.producer.ProducerRecord;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@RestController
@RequestMapping("/cartao")
public class CartaoController {

	@Autowired
	private CartaoService cartaoService;

	@Autowired
	private ClienteClient clienteClient;
	
	@Autowired
	private KafkaTemplate<String, String> template;

	Logger logger = LoggerFactory.getLogger(CartaoController.class);

	@Value("${sleep:10}")
	Long sleep;

	@PostMapping
	@ResponseStatus(code = HttpStatus.CREATED)
	public Cartao criar(@Valid @RequestBody Cartao cartao, Principal principal) throws JsonProcessingException {

		cartao.setCpfCliente(principal.getName());
		
		ObjectMapper mapper = new ObjectMapper();
		template.send(new ProducerRecord<String, String>("criar-cartao", "1", mapper.writeValueAsString(cartao)));


		return cartaoService.criar(cartao);
	}

	@PatchMapping("/{numero}/ativar")
	public void ativar(@PathVariable String numero) {
		Optional<Cartao> optional = cartaoService.buscar(numero);

		if (!optional.isPresent()) {
			throw new ResponseStatusException(HttpStatus.NOT_FOUND);
		}

		cartaoService.ativar(numero);
	}

	@GetMapping("/{numero}")
	public Cartao buscar(@PathVariable String numero) {
		Optional<Cartao> optional = cartaoService.buscar(numero);

		if (!optional.isPresent()) {
			throw new ResponseStatusException(HttpStatus.NOT_FOUND);
		}

		return optional.get();
	}

	@GetMapping("/teste")
	public String teste() throws InterruptedException {
		Thread.sleep(sleep);
		return "funcionou";
	}

//	@HystrixCommand(fallbackMethod = "contingenciaBuscarCliente")
//	@GetMapping("/cliente/{id}")
//	public Optional<Cliente> buscar(@PathVariable("id") int id) {
//		return clienteClient.buscar(id);
//	}
//
//	public String contingenciaBuscarCliente(Throwable e) {
//		logger.error(e.getMessage());
//		return "Eu sou a contingência";
//	}

}
