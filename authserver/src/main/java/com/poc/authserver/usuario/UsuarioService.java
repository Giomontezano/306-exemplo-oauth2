package com.poc.authserver.usuario;

import java.util.Collections;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

@Service
public class UsuarioService implements UserDetailsService {

	@Autowired
	private UsuarioRepository repository;
	
	@Autowired
	private BCryptPasswordEncoder encoder;

	public void criarUsuario(String cpf, String senha) {
		Usuario usuario = new Usuario();
		usuario.setCpf(cpf);
		usuario.setSenha(encoder.encode(senha));

		repository.save(usuario);
	}
	
	
	public void criarCartao(String cpf) {
		Usuario usuario = new Usuario();
		usuario.setCpf(cpf);

		repository.save(usuario);
	}

	@Override
	public UserDetails loadUserByUsername(String cpf) throws UsernameNotFoundException {
		Optional<Usuario> optional = repository.findByCpf(cpf);

		if (!optional.isPresent()) {
			throw new UsernameNotFoundException("Usuário não encontrado");
		}

		Usuario usuario = optional.get();

		SimpleGrantedAuthority authority = new SimpleGrantedAuthority("user");
		return new User(usuario.getCpf(), usuario.getSenha(), Collections.singletonList(authority));

	}
}
