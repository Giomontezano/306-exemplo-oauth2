package com.poc.authserver.cartao;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.poc.authserver.usuario.Usuario;
import com.poc.authserver.usuario.UsuarioController;
import com.poc.authserver.usuario.UsuarioService;


@RestController
public class CartaoController {

	@Autowired
	private UsuarioService usuarioService;

	Logger logger = LoggerFactory.getLogger(UsuarioController.class);


	@PostMapping("/cartao")
	public void criarCartao(@RequestBody Usuario usuario) {
		usuarioService.criarCartao( usuario.getCpf() );
		
	}

}
