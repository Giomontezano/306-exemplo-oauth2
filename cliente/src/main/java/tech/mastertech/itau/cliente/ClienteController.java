package tech.mastertech.itau.cliente;

import java.util.Optional;

import javax.validation.Valid;

import org.apache.kafka.clients.producer.ProducerRecord;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.databind.ObjectMapper;

@RestController
public class ClienteController {

	@Autowired
	private ClienteService clienteService;

	@Autowired
	private AuthClient authClient;

	@Autowired
	private KafkaTemplate<String, String> template;

	@PostMapping("/cliente")
	@ResponseStatus(code = HttpStatus.CREATED)
	public Cliente criar(@Valid @RequestBody Cliente cliente) throws Exception {

		authClient.criarUsuario(cliente);

		ObjectMapper mapper = new ObjectMapper();
		template.send(new ProducerRecord<String, String>("criar-cliente", "1", mapper.writeValueAsString(cliente)));

		return cliente;
	}

	@GetMapping("/cliente/{id}")
	public Optional<Cliente> buscar(@PathVariable("id") int id) {
		return clienteService.buscar(id);
	}
	
	@GetMapping("/buscar/{cpf}")
	  public Optional<Cliente> buscar(@PathVariable String cpf) {
	    Optional<Cliente> cliente = clienteService.buscarPorCpf(cpf);

	    if (cliente.isPresent()) {
	      return cliente;
	    }
	      
	    return Optional.empty();
	  }


}
