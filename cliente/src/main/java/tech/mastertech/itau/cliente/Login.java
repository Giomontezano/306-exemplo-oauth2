package tech.mastertech.itau.cliente;

import javax.validation.constraints.NotBlank;

import org.hibernate.validator.constraints.br.CPF;

public class Login {
  @CPF
  @NotBlank
  private String cpf;
  
  @NotBlank
  private String senha;

  public String getCpf() {
    return cpf;
  }

  public void setCpf(String cpf) {
    this.cpf = cpf;
  }

  public String getSenha() {
    return senha;
  }

  public void setSenha(String senha) {
    this.senha = senha;
  }
}
