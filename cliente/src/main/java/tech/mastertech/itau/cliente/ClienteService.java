package tech.mastertech.itau.cliente;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ClienteService {
	@Autowired
	private ClienteRepository clienteRepository;

	public Optional<Cliente> buscar(int id) {
		return clienteRepository.findById(id);
	}

	public Optional<Cliente> login(Login login) {
		Optional<Cliente> optional = clienteRepository.findByCpf(login.getCpf());

		if (!optional.isPresent()) {
			return optional;
		}

		Cliente cliente = optional.get();

//    if(passwordEncoder.matches(login.getSenha(), cliente.getSenha())) {
//      return optional;
//    }

		return Optional.empty();
	}

	public Optional<Cliente> buscarPorCpf(String cpf) {
		return clienteRepository.findByCpf(cpf);
	}

}
