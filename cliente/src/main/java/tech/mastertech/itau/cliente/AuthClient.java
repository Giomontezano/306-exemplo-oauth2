package tech.mastertech.itau.cliente;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

@FeignClient(name = "authserver")
public interface AuthClient {

	@PostMapping("/usuario")
	public void criarUsuario(@RequestBody Cliente cliente);
}
