package tech.master.alunos;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

@RestController
public class TurmaController {
  @Autowired
  private TurmaService service;
  
  @GetMapping
  public Iterable<Turma> listar(){
    return service.listar();
  }
  
  @PostMapping("/{id}")
  public Turma criar(@PathVariable int id) {
    Turma turma = service.criar(id);
    
    if(turma == null) {
      throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
    }
    
    return turma;
  }
}
